
# MNase_QC - ImageJ macro tool
* [ImageJ installation](#ImageJ installation)  
* [MNase_QC macro installation](#MNase_QC macro installation)  
* [Image preparation](#Image preparation)  
* [Image processing](#Image processing)  
* [Results interpretaton](#Results interpretation)  
* [Settings](#Settings)  
* [Multiple measurements and detection of the standard deviation](#Multiple measurements and detection of the standard deviation)  

**MNase_QC** is an ImageJ macro tool that process gel electrophoresis images after MNase digestion of chromatin.  
The tool estimates ratios of mono-nuceosomes as well as detects over- and under-digested 
DNA fragments to find optimal digestion conditions to receive good quality samples for the further usage in High-throughput 
sequencing techniques, such as MNase-seq.  

# ImageJ installation
To run the current macro the ImageJ should be installed.  
There are two versions, the ImageJ itself and its extended version called FIJI.  
To install Fiji follow the link http://fiji.sc/ and download.  
To install ImageJ, follow the link http://rsbweb.nih.gov/ij/download.html and download.  
ImageJ denepds on Java that should be also installed.  
If you have problems installing on Mac, please [look here](http://microscopynotes.com/imagej/getting_started/installingOSX/index.html).  

# MNase_QC macro installation
To install **MNase_QC** macro download it from the [https://gitlab.com/mglab/mnase_qc](https://gitlab.com/mglab/mnase_qc) and move the *MNase_QC.ijm* file to the *ImageJ/macro* directory.  
Then open ImageJ/Fiji and follow *Plugins > Macros > Install..* to choose the *MNase_QC.ijm* file.  
To run installed **MNase_QC** macro you can click on *Plugins > Macros*.  
There you should see two installed macros "MNase_QC - Settings" and "MNase_QC - Processing" (see Figure 1).  

![image](img/img_01.png)  
Figure 1. Installed MNase_QC macro tool.  

It is also possible to install **MNase_QC** permanently. For doing this, insert content of *MNase_QC.ijm* macro into the file named "StartupMacros.txt" in the *ImageJ/macros* directory to start macros at the ImageJ start up.  

# Image preparation
The good quality image is a prerequisite of the good quality control estimation.  
1. Recommended parameters: 
- 15 µg of MNase digested pure DNA per lane.  
- 50 bp DNA Ladder (NEB, N3236)  
- Gel percentage in TBE buffer  
- Chamber  
- Run gele for 6h at 90V, temperature,  etc.  
2. Staining, ethidium bromid concentration and incubation time.  
3. Saving image: dark background, 8-16 bits, saturation control, tiff format etc.  

# Image processing
1. Launch ImageJ.
2. Make sure that **MNase_QC** macro tool is installed or install it as described above.  
3. Open a gel electrophoresis image in *File > Open...* or drag-and-drop the image to the bottom panel of the ImageJ.  
Note that background of the image should be dark with light lanes. In the case of the light background you can activate '*invert background color*' checkbox in Settings (described below).  
4. Select lane using rectangular selection tool as an Region Of Interest (ROI) from the upper (XX bp) to the lower (XX bp) marker bands (see Figure 2).  
5. Add selection to the ROI Manager by pressing '**t**' or by using *Tools > ROI Manager > Add*.  
6. Rename selected ROI to remember the selected lane using *Rename* field in the ROI Manager.  
7. Add selections to the ROI Manager for all lanes to process.  
8. Process selected lanes by pressing '**u**' key on a keyboard or in *Plugins > Macros > MNase_QC Processing [u]*.  

![image](img/img_02.png)  
Figure 2. Selection the Region Of Interest (ROI).  

# Results interpretation
MNase_QC macro generates several files and save them in a separate directory where input image is stored.  
Saved files:  
1. ..ratio.csv - Table with ratios for mono-, over- and underdigested fragments for all selected lanes.  
2. ..profile.png - Profile diagram with selected thresholds for mono-, over- and underdigested fragments. It is important to visually control that mononucleosomal peak was correctrly chosen (see Figure 3).  
3. ..profile.csv - contains raw profile intensities for selected coordinates.  
4. ..peaks.csv - Table with x-coordinates for chosen thresholds.  
5. ..processed.tiff - Processed image on the final stage that was used for the quantification.  

![image](img/img_03.png)  
Figure 3. Profile diagram for a single lane with selected thresholds for mono-, over- and underdigested fragments.  

**MNase QC** calculates ratio of mononucleosomes, under- and over-digested fragments.  
The optimal parameters will be detected and reported here later.  

# Settings
The settings and parameters used in **MNase_QC** tool can be controled in *Plugins > Macros > MNase_QC - Settings* before processing step.
For the background subtraction, the standard ImageJ method Rolling ball is used with the default rolling ball size '100 px'.  
It is possible invert image colors for the images with the light background.  
Peak selection is an important step during the image processing. The amplitude of the selected peaks and bottoms can be also controlled to select the first nucleosome peak properly in the case it was not selected correctly during processing using standard parameters.  
Both *Minimal peak amplitude* and *Peak width* parameters are added to resolve doubled the first peak.  
It is also possible to control the data which will be saved to reduce space on the disk if further processing is not planed.  

![image](img/img_04.png)  
Figure 4. Settings window.  

# Multiple measurements and detection of the standard deviation
**MNase QC** does not calculate statistical parameters for multiple measurements.  
It is possible, however, to quantify them using multiple selections for a single lane and naming them e.g. 'lane1_rep1', 'lane1_rep2' and so on.  
After quantification, open the file with ratios in any table reading tool of your choice such as Excel, LibreOffice calc or Google Sheets. Calculate needed statistical parameters such as average and standard deviations for multiple measurements.  


