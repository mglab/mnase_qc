/* 
 * MNase_QC.ijm
 * 
 * https://gitlab.com/mglab/mnase_qc
 * 
 * ImageJ macro process gel electrophoresis images
 * after MNase digestion of chromatin. 
 * The macro estimates ratios of mono-nuceosomes 
 * as well as over- and under-digested DNA fragments 
 * to find optimal digestion conditions for successful 
 * High-throughput sequencing techniques, such as MNase-seq.
 * For detailed usage see README.
 * Author: Mark Boltengagen (m.boltengagen@gmail.com)
 * Date: 2023-04-11
 */

// Parameters & Settings
var subtract_bg = 1;        // Subtract background using rolling ball or not
var rolling = 100;          // Rolling ball argument for background subtraction
var invert_colors = 0;      // Invert image colors or not
var peak_amplitude = 10;    // Finding peaks min amplitude
var save_raw_data = 1;      // Save peaks and profile data in csv file or not
var save_processed_img = 1; // Save processed image or not
var peak_width = 30;        // Half-width of the peak
var min_peak_amplitude = 2; // Minimal peak amplitude to resolve doubled first peak
var verbose = 1;            // Show messages or not

// Calculate sum of all elements in a numeric array
function sumArray(array) {
    sum = 0;
    for (i=0; i<array.length; i++){
        sum = sum + array[i];
    }
    return sum;
}

// Save array into file
function saveArray(array, file) {
    for (i=0; i<array.length; i++) {
        File.append(i +"\t"+ array[i], file);
    }
}


// Main processing of the image
macro "MNase_QC - Processing [u]" {
    
    // is ROIs selected?
    nROIs = roiManager("count");
    if (nROIs == 0) {
        exit("The ROI Manager is empty. Please select lanes using rectangle selection tool.");
    }
    
    // Get image name
    image_name = File.getNameWithoutExtension(Image.title());

    // Output directory
    outdir = getDirectory("image") + image_name + "_mnase_qc";
    File.makeDirectory(outdir);
    // CSV file to append calculated ratios
    file = outdir + "/" + image_name + "_ratio.csv";
    if (File.exists(file)) {
        File.delete(file);
    }
    File.append("Lane_id\tMono\tUnder\tOver", file);
    
    // CSV file to append calculated peaks
    if (save_raw_data) {
        peaks = outdir + "/peaks.csv";
        if (File.exists(peaks)) {
            File.delete(peaks);
        }
        File.append("Lane_id\tunder_idx\tfirst_idx\tover_idx", peaks);
    }
    
    // Rotate image to place lanes horizontally
    run("Rotate 90 Degrees Left");
    
    // Invert image color
    if (invert_colors) {
        run("Invert");
    }
    
    // Subtract Background
    if (subtract_bg) {
        run("Subtract Background...", "rolling="+rolling);
    }
    
    // Save processed image
    if (save_processed_img) {
        saveAs("tiff", outdir + "/" + image_name + "_processed.tiff");
    }
    
    // Peak calling and AUC calculation

    for (n = 0; n < nROIs; n++) {
        roiManager("select", n);
        roi_name = RoiManager.getName(n);
        
        // Update ROI for the rotated image
        getSelectionBounds(x_roi, y_roi, w_roi, h_roi);
        makeRectangle(y_roi, getHeight() - x_roi - w_roi, h_roi, w_roi);
        roiManager("update");
        
        // Get Profile data
        y = getProfile();
        x = Array.getSequence(y.length);
        
        // Save profile data
        if (save_raw_data) {
            // CSV file to append profile data
            profile = outdir + "/" + image_name + "_" + roi_name + "_profile.csv";
            if (File.exists(profile)) {
                File.delete(profile);
            }
            File.append("Coordinate\tIntensity", profile);
            // Append intensity per coordinate (profile data)
            saveArray(y, profile);
        }
        
        // Get peaks
        max = Array.findMaxima(y, peak_amplitude, 1);
        // Get maximal peak assumed mononucleosomes
        max = Array.sort(max);
        if (max.length == 0) {
            if (verbose) {
                Dialog.create("Warning");
                Dialog.addMessage("The first peak for the " + roi_name + " was not detected. "
                                  +"Try to reduce peak amplitude in Settings. \n \n"
                                  +"Press OK to continue calculations for other ROIs.");
                Dialog.show();
            }
            continue; // next ROI
        }
        first_idx = max[max.length-1];
        
        // Get bottoms
        min = Array.findMinima(y, peak_amplitude, 1);
        // Get maximal bottom assumed between mono- and di-nucleosomes
        min = Array.sort(min);
        if (min.length == 0) {
            if (verbose) {
                Dialog.create("Warning");
                Dialog.addMessage("Bottom for the " + roi_name + " was not detected. "
                                  +"Try to reduce peak amplitude in Settings. \n \n"
                                  +"Press OK to continue calculations for other ROIs.");
                Dialog.show();
            }
            continue; // next ROI
        }
        under_idx = min[min.length-1];
        
        // Set border of overdigested
        over_idx = round(first_idx+(first_idx-under_idx)/2);
        if (over_idx > y.length-1) {
            over_idx = y.length-1;
        }
        
        // Resolve doubled first peak
        // Check borders
        left_idx = first_idx - peak_width;
        if (left_idx < 0) {
            left_idx = 0;
        }
        right_idx = first_idx + peak_width;
        if (right_idx > y.length-1) {
            right_idx = y.length-1;
        }
        // Array for the first peak
        y_first = Array.slice(y, left_idx, right_idx);
        
        step = 1;
        // Increase step if amplitude set high
        if (peak_amplitude > 10) {
            step = round(0.1*peak_amplitude);
        }
        
        // Update thresholds if doubled first peak
        for (amplitude_i = peak_amplitude; amplitude_i <= min_peak_amplitude; amplitude_i -= step) {
            max_first = Array.findMaxima(y_first, amplitude_i, 1);
            if (max_first.length == 2) {
                min_first = Array.findMinima(y_first, amplitude_i, 1);
                if (min_first.length == 1) {
                    // Set updated the first peak and overdigested threshold
                    under_idx = min_first[0];
                    max_first = Array.sort(max_first);
                    first_idx = max_first[0];
                    break;
                }
            }
        }
        
        if (save_raw_data) {
            // Append calculated peaks to the file
            File.append(roi_name +"\t"+ under_idx +"\t"+ first_idx +"\t"+ over_idx, peaks);
        }
        
        // Plot lane profile
        Plot.create("Profile Plot", "Distance in Pixels", "Intensity");
        Plot.setColor("black");
        Plot.setLineWidth(2);
        Plot.add("line", x, y);
        Plot.setFontSize(14);
        
        // Show threshold for mono-nucleosomal peak
        Plot.setColor("#808080");
        Plot.drawLine(first_idx, 0, first_idx, y[first_idx]);
        // Show threshold between mono- and di-nucleosomes
        Plot.setColor("red");
        Plot.drawLine(under_idx, 0, under_idx, y[under_idx]);
        // Show threshold for overdigested fragments
        Plot.drawLine(over_idx, 0, over_idx, y[over_idx]);
        // Plot circles for thresholds
        xc = newArray(under_idx, over_idx, first_idx);
        yc = newArray(y[under_idx], y[over_idx], y[first_idx]);
        Plot.add("circle", xc, yc);
        Plot.show();
        
        // Save plot with chosen thresholds as an image
        selectWindow("Profile Plot");
        saveAs("png", outdir + "/" + image_name + "_" + roi_name + "_profile.png");
        close(image_name + "_" + roi_name + "_profile.png");
        
        // Calculate ratios
        all = sumArray(y);
        // division to zero
        if (all == 0) {
            if (verbose) {
                Dialog.create("Warning");
                Dialog.addMessage("AUC for the " + roi_name + " is equal to zero. "
                                  +"Rates of mononuceosomes, over- and under-digested fragments "
                                  +"can not be calculated. Please check if selected ROI has visible signal." \n \n"
                                  +"Press OK to continue calculations for other ROIs.");
                Dialog.show();
            }
            continue; // next ROI
        }
        
        mono_array = Array.slice(y, under_idx, over_idx);
        mono = sumArray(mono_array)/all;
        //TODO Check that empty array can lead to empty sum or error?
        
        over_array = Array.slice(y, over_idx, y.length-1);
        over = sumArray(over_array)/all;
        
        under_array = Array.trim(y, under_idx);
        under = sumArray(under_array)/all;
        
        // Append ratios to the file
        File.append(roi_name +"\t"+ mono +"\t"+ under +"\t"+ over, file);
    }
    roiManager("reset");
    close("ROI Manager");
    close();
} // end of macro


// Preprocessing GUI (Optional)
macro "MNase_QC - Settings [U]" {
    // Preprocessing settings
    Dialog.create("MNase QC Settings");
    Dialog.addMessage("Background correction parameters:");
    Dialog.addCheckbox("Subtract background", subtract_bg);
    Dialog.addNumber("Rolling ball radius:", rolling);
    Dialog.addMessage("Image preparation:");
    Dialog.addCheckbox("Invert image color", invert_colors);
    Dialog.addMessage("Saving data:");
    Dialog.addCheckbox("Save processed image" , save_processed_img);
    Dialog.addCheckbox("Save raw profile data and peaks", save_raw_data);
    Dialog.addCheckbox("Show warning messages", verbose);
    Dialog.addMessage("Peak selection parameter:");
    Dialog.addNumber("Peak amplitude:", peak_amplitude);
    Dialog.addNumber("Peak width" , peak_width);
    Dialog.addNumber("Minimal peak amplitude", min_peak_amplitude);
    Dialog.show();
    
    subtract_bg = Dialog.getCheckbox();
    rolling = Dialog.getNumber();
    invert_colors = Dialog.getCheckbox();
    save_processed_img = Dialog.getCheckbox();
    save_raw_data = Dialog.getCheckbox();
    verbose = Dialog.getCheckbox();
    peak_amplitude = Dialog.getNumber();
    peak_width = Dialog.getNumber();
    min_peak_amplitude = Dialog.getNumber();
    
    // Check validity
    if (rolling <= 0) {
        rolling = 100;
        exit("Rolling ball radius should be positive number bigger than 0. Please insert a valid number.");
    }
    if (peak_amplitude < 1) {
        peak_amplitude = 10;
        exit("Peak amplitude should be a positive number bigger or equal to 1. Please insert a valid number.");
    }
    if (peak_width < 1) {
        peak_width = 30;
        exit("Peak width should be a positive number bigger or equal to 1. Please insert a valid number.");
    }
    if (min_peak_amplitude < 1) {
        min_peak_amplitude = 2;
        exit("Minimal peak amplitude should be a positive number bigger or equal to 1. Please insert a valid number.");
    }
} // end of macro

